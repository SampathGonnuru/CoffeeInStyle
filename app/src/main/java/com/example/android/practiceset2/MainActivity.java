
package com.example.android.practiceset2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    int quantity = 0;
    int whippedcream = 0;
    int chocolate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * this method is used for incrementing the + of orders given
     */

    public void increment(View view) {
        if (quantity == 100) {
            //toast for excessive coffees
            Toast.makeText(this, "You cannot have more than 100 coffees", Toast.LENGTH_SHORT).show();
            //exit
            return;
        }
        quantity = quantity + 1;
        display(quantity);
    }

    /**
     * this method is used for decrementing the number of orders submitted
     */

    public void decrement(View view) {
        if (quantity == 1) {
            //make a warning message as toast for negative coffee orders
            Toast.makeText(this, "You cannot have less than 1 coffees", Toast.LENGTH_SHORT).show();
            //exit as it is just a pop-up
            return;
        }
        quantity = quantity - 1;
        display(quantity);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        // Find the user's name
        EditText naameField = (EditText) findViewById(R.id.naame_field);
        String naame = naameField.getText().toString();

        //for adding whipped cream
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        boolean hasWhippedCream = whippedCreamCheckBox.isChecked();

        //for adding chocolate
        CheckBox ChocolateCheckBox = (CheckBox) findViewById(R.id.chocolate_checkbox);
        boolean hasChocolate = ChocolateCheckBox.isChecked();


        int price = calculatePrice(hasWhippedCream, hasChocolate);
        String name = createOrderSummary(naame, price, hasWhippedCream, hasChocolate);
        displayMessage(name);


    }

    /**
     * calculates price of the order.
     *
     * @param addWhippedCream is wether or not user wants whipped cream
     * @param addChocolate    is wether or not user wants chocolate topping
     * @return total price
     */

    private int calculatePrice(boolean addWhippedCream, boolean addChocolate) {
        //price of 1 cup of coffee
        int basePrice = 5;

        //Add $1 if user wants whippedcream
        if (addWhippedCream) {
            basePrice = basePrice + 1;
        }

        //Add $2 if user wants Chocolate
        if (addChocolate) {
            basePrice = basePrice + 2;
        }

        //Total price
        return quantity * basePrice;
    }


    /**
     * create summary of the order.
     *
     * @param naame
     * @param price
     * @param addWhippedCream
     * @param addchocolate
     * @return text summary
     */

    private String createOrderSummary(String naame, int price, boolean addWhippedCream, boolean addchocolate) {
        String name = "Name: " + naame;
        name += "\nAdd whipped cream? " + addWhippedCream;
        name += "\nAdd chocolate? " + addchocolate;
        name += "\nQuantity: " + quantity;
        name+= "\nTotal: $" + price;
        name += "\n Thank you for buying @starbucks!";
        return name;
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }


    /**
     * This method displays the given text on the screen.
     */
    private void displayMessage(String message) {
        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        orderSummaryTextView.setText(message);
    }
}